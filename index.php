<?php

require_once './vendor/autoload.php';

require_once 'sqlhelp.php';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);

$header = $twig->load("header.html");
echo $header-> render ('header.html',array(
      'page' => 'Assignment'
));


$bar = $twig->load("bar.html");
echo $bar-> render ('bar.html',array(
      'pokemon1' => 'one',
      'pokemon2' => 'two',
      'pokemon3' => 'three',
      'pokemon4' => 'four'
));

?>