Create Database pokedex;

Create Table pokemon(
    id int NOT NULL,
    nat int,
    names text,
    hp int,
    attack int,
    defence int,
    special_attack int,
    special_defence int,
    speed int,
    base_stat_total int,
    Primary key (id)

);

Insert Into pokemon (id,nat,names,hp,attack,defence,special_attack,special_defence,speed,base_stat_total) Values
(1,152,'Chikorita',45,49,65,49,65,45,318),
(2,153,'Bayleef',60,62,80,63,80,60,405),
(3,154,'Meganium',80,82,100,83,100,80,525),
(4,155,'Cynadaquil',39,52,43,60,50,65,309),
(5,156,'Quilava',58,64,58,80,65,80,405),
(6,157,'Typhlosion',78,84,78,109,85,100,534),
(7,158,'Totodile',50,65,64,44,48,43,314),
(8,159,'Croconaw',65,80,80,59,63,58,405),
(9,160,'Feraligatr',85,105,100,79,83,78,530),
(10,161,'Sentret',35,46,34,35,45,20,215),
(11,162,'Furret',85,76,64,45,55,90,415),
(12,163,'Hoothoot',60,30,30,36,56,50,262),
(13,164,'Noctowl',100,50,50,76,96,70,442),
(14,165,'Ledyba',40,20,30,40,80,55,265),
(15,166,'Ledian',55,35,50,55,110,85,390),
(16,167,'Spinarak',40,60,40,40,40,30,250),
(17,168,'Ariados',70,90,70,60,60,40,390),
(18,169,'Crobat',85,90,80,70,80,130,535),
(19,170,'Chinchou',75,38,38,56,56,67,330),
(20,171,'Lanturn',125,58,58,76,76,67,460),
(21,172,'Pichu',20,40,15,35,35,60,205),
(22,173,'Cleffa',50,25,28,45,55,15,218),
(23,174,'Igglybuff',90,30,15,40,20,15,210),
(24,175,'Togepi',35,20,65,40,65,20,245),
(25,176,'Togetic',55,40,85,80,105,40,405),
(26,177,'Natu',40,50,45,70,45,70,320),
(27,178,'Xatu',65,75,70,95,70,95,470),
(28,179,'Mareep',55,40,40,65,45,35,280),
(29,180,'Flaaffy',70,55,55,80,60,45,365),
(30,181,'Ampharos',90,75,85,115,90,55,510),
(31,181,'Mega Ampharos',90,95,105,165,110,45,610),
(32,182,'Bellossom',75,80,95,90,100,50,490),
(33,183,'Marill',70,20,50,20,50,40,250),
(34,184,'Azumarill',100,50,80,60,80,50,420),
(35,185,'Sudowoodo',70,100,115,30,65,30,410),
(36,186,'Politoed',90,75,75,90,100,70,500),
(37,187,'Hoppip',35,35,40,35,55,50,250),
(38,188,'Skiploom',55,45,50,45,65,80,340),
(39,189,'Jumpluff',75,55,70,55,95,110,460),
(40,190,'Aipom',55,70,55,40,55,85,360),
(41,191,'Sunkern',30,30,30,30,30,30,180),
(42,192,'Sunflora',75,75,55,105,85,30,425),
(43,193,'Yanma',65,65,45,75,45,95,390),
(44,194,'Wooper',55,45,45,25,25,15,210),
(45,195,'Quagsire',95,85,85,65,65,35,430),
(46,196,'Espeon',65,65,60,130,95,110,525),
(47,197,'Umbreon',95,65,110,60,130,65,525),
(48,198,'Murkrow',60,85,42,85,42,91,405),
(49,199,'Slowking',95,75,80,100,110,30,490),
(50,200,'Misdreavus',60,60,60,85,85,85,435),
(51,201,'Unown',48,72,48,72,48,48,336),
(52,202,'Wobbuffet',190,33,58,33,58,33,405),
(53,203,'Girafarig',70,80,65,90,65,85,455),
(54,204,'Pineco',50,65,90,35,35,15,290),
(55,205,'Forretress',75,90,140,60,60,40,465),
(56,206,'Dunsparce',100,70,70,65,65,45,415),
(57,207,'Gligar',65,75,105,35,65,85,430),
(58,208,'Steelix',75,85,200,55,65,30,510),
(59,208,'Mega Steelix',75,125,230,55,95,30,610),
(60,209,'Snubble',60,80,50,40,40,30,300),
(61,210,'Granbull',90,120,75,60,60,45,450),
(62,211,'Qwilfish',65,95,75,55,55,85,430),
(63,212,'Scizor',70,130,100,55,80,65,500),
(64,212,'Mega Scizor',70,150,140,65,100,75,600),
(65,213,'Shuckle',20,10,230,10,230,5,505),
(66,214,'Heracross',80,125,75,40,95,85,500),
(67,214,'Mega Heracross',80,185,115,40,105,75,600),
(68,215,'Sneasel',55,95,55,35,75,115,430),
(69,216,'Teddiursa',60,80,50,50,50,40,330),
(70,217,'Ursaring',90,130,75,75,75,55,500),
(71,218,'Slugma',40,40,40,70,40,20,250),
(72,219,'Magcargo',50,50,120,80,80,30,410)
);
