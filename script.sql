/* query to select top 10 pokemon */

select names,defence
from pokemon
order by defence desc
limit 10;

/*ques2 */
select * from pokemon, go_type1 where
go_type1.typeName =='Electric' and pokemon.id == num.id and
num.first_type  =  go_type1.strong or
num.sec_type = go_type1.strong;

/*ques 3 */
select * from pokemon, go_type1 where
go_type1.typeName =='Ice' and pokemon.id == num.id and
num.first_type  =  go_type4.strong or
num.sec_type = go_type4.strong;

/*ques 4 */
select * from pokemon, go_type1, go_type2
where pokemon.base_stat_total > 300 and pokemon.base_stat_total < 400
and go_type1.typeName =='Flying' and
go_type2.typeName =='Grass'  and pokemon.id == num.id and
num.first_type  =  go_type1.strong or
num.sec_type = go_type1.strong;

/*ques 5 */
select * from pokemon, go_type1, max(attack)
where go_type1.typeName = 'Psychic' and
pokemon.id == num.id and
num.first_type  =  go_type1.strong or
num.sec_type = go_type1.strong;
